open Environment


type integer_arithmetic 			= Add	| Sub | Mul | Div	| Mod
type boolean_arithmetic 			= And | Or
type unary_boolean_arithmetic = Not
type comparison_arithmetic		= Gt 	| Lt	| Geq | Leq	| Eq	| Neq

type bit_arithmetic = Bit_And | Bit_Or | Bit_Xor | Bit_Shift_Left | Bit_Shift_Right
type unary_bit_arithmetic = Bit_Not

type exp =
	| Number 													of int
	| Boolean 												of bool
	| Id															of string
	| Function 												of string 									* exp
	| Integer_Arithmetic 							of integer_arithmetic 			* exp * exp
	| Boolean_Arithmetic 							of boolean_arithmetic 			* exp * exp
	| Unary_Boolean_Arithmetic 				of unary_boolean_arithmetic * exp
	| Bitwise_Arithmetic 							of bit_arithmetic		 				* exp * exp
	| Unary_Bitwise_Arithmetic				of unary_bit_arithmetic			* exp
	| Comparison				 							of comparison_arithmetic		* exp * exp
	| Conditional_Expression 					of 											exp * exp * exp
	| Sequence_Expression 						of 											exp * exp
	| Named_Function									of string * string 					* exp
	| Block														of exp list									* exp
	| Declaration 										of string			 							* exp
	| Reference_Assignment_Expression of											exp * exp
	| Dereferenciation_Expression 		of														exp
	| Function_Application						of 											exp * exp
	| Record													of									(string * exp) list
	| Select													of	 													exp * string
	| Print														of 														exp
	| Println													of 														exp

exception SyntaxError of string * string

let rec unparse a =
	match a with
	| Number n  ->
			"Num(" ^string_of_int n^")"

	| Boolean b ->
			"Bool("^string_of_bool b^")"

	| Id id ->
			"Id("^id^")"

	| Function (param,exp) ->
			"Function("^
				param^","^(unparse exp)
			^")"

	| Named_Function (name, param, body) ->
			"NamedFunction("^
				name^","^param^","^(unparse body)
			^")"

	| Integer_Arithmetic 	(op,l,r) ->
			(unparse_integer_arithmetic op)^ "(" ^
				(unparse l)^","^(unparse r)
			^")"

	| Boolean_Arithmetic 	(op,l,r) ->
			(unparse_boolean_arithmetic op)^ "(" ^
				(unparse l)^","^(unparse r)
			^")"

	| Unary_Boolean_Arithmetic (op, exp) ->
			(unparse_unary_boolean_arithmetic op)^ "(" ^
				(unparse exp)
			^")"

	| Bitwise_Arithmetic 	(op,l,r) ->
			(unparse_bitwise_arithmetic op)^ "(" ^
				(unparse l)^","^(unparse r)
			^")"

	| Unary_Bitwise_Arithmetic (op, exp) ->
		(unparse_unary_bitwise_arithmetic op)^ "(" ^
			(unparse exp)
		^")"

	| Comparison 					(op,l,r) ->
			(unparse_comparison op)^ "(" ^
				(unparse l)^","^(unparse r)
			^")"
	| Conditional_Expression (cond,l,r) ->
			"ConditionalExpression(" ^
				(unparse cond)^","^(unparse l)^","^(unparse r)
			^")"
	| Sequence_Expression (l,r) ->
			"SequenceExpression(" ^
				(unparse l)^","^(unparse r)
			^")"
	| Block (decls,exp) ->
			"Block("^
				(unparse_declaration_list decls) ^ "," ^ (unparse exp)
			^")"

	| Declaration (id,exp) ->
			"Declaration("^
				id ^ "," ^ (unparse exp)
			^")"

	| Reference_Assignment_Expression (l,r) ->
			"ReferenceAssignmentExpression(" ^
				(unparse l)^","^(unparse r)
			^")"
	| Dereferenciation_Expression (exp) ->
			"DereferenciationExpression(" ^
						(unparse exp)
			^")"
	| Function_Application (f, a) ->
			"FunctionApplication(" ^
						(unparse f) ^ "," ^ (unparse a)
			^ ")"

	| Record (r) ->
			"Record(" ^
						(unparse_record r)
			^ ")"

	| Select (exp,field) ->
			"Select(" ^
						(unparse exp) ^ "," ^ field
			^ ")"

	| Print (exp) ->
			"Print(" ^
						(unparse exp)
			^ ")"
	| Println (exp) ->
			"Println(" ^
						(unparse exp)
			^ ")"


and unparse_integer_arithmetic op =
	match op with
	| Add -> "Add"
	| Sub -> "Sub"
	| Mul -> "Mul"
	| Div -> "Div"
	| Mod -> "Mod"

and unparse_boolean_arithmetic op =
	match op with
	| And -> "And"
	| Or 	-> "Or"

and unparse_unary_boolean_arithmetic op =
	match op with
	| Not -> "Not"

and unparse_comparison op =
	match op with
	| Gt 	-> "Gt"
	| Lt 	-> "Lt"
	| Geq -> "Geq"
	| Leq	-> "Leq"
	| Eq 	-> "Eq"
	| Neq -> "Neq"

and unparse_bitwise_arithmetic op =
	match op with
	| Bit_And 				-> "Bit_And"
	| Bit_Or 					-> "Bit_Or"
	| Bit_Xor 				-> "Bit_Xor"
	| Bit_Shift_Left 	-> "Bit_Shift_Left"
	| Bit_Shift_Right -> "Bit_Shift_Right"

and unparse_unary_bitwise_arithmetic op =
	match op with
	| Bit_Not 				-> "Bit_Not"

and unparse_scoped_declaration_list l =
	match l with
	| [] -> ""
	| [a] -> unparse a
	| x::xs -> (unparse x) ^ "," ^ unparse_scoped_declaration_list xs

and unparse_declaration_list l = unparse_scoped_declaration_list l

and unparse_record l =
	match l with
	| [] -> ""
	| [(id,exp)] -> (unparse (Declaration(id,exp)))
	| (id,exp)::xs -> (unparse (Declaration(id,exp))) ^ "," ^ (unparse_record xs) 
